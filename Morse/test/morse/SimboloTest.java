/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ferreira
 */
public class SimboloTest {
    
    public SimboloTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }



    /**
     * Test of isLetter method, of class Simbolo.
     */
    @Test
    public void testIsLetter() {
        System.out.println("isLetter");
        Simbolo instance = new Simbolo(null, "E", "LETTER");
        boolean expResult = true;
        boolean result = instance.isLetter();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of isNumber method, of class Simbolo.
     */
    @Test
    public void testIsNumber() {
        System.out.println("isNumber");
        Simbolo instance = new Simbolo(null, "7", "NUMBER");
        boolean expResult = true;
        boolean result = instance.isNumber();
        assertEquals(expResult, result);
 
    }

    /**
     * Test of isNonEnglish method, of class Simbolo.
     */
    @Test
    public void testIsNonEnglish() {
        System.out.println("isNonEnglish");
        Simbolo instance = new Simbolo(null, "CH", "Non-English");
        boolean expResult = true;
        boolean result = instance.isNonEnglish();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of classificaChar method, of class Simbolo.
     */
    @Test
    public void testClassificaChar() {
        System.out.println("classificaChar");
        int[] classificaChar = new int [4];
        classificaChar[0]=1;
        classificaChar[1]=0;
        classificaChar[2]=0;
        classificaChar[3]=0;
        Simbolo instance = new Simbolo(null, null, "LETTER");
        instance.classificaChar(classificaChar);
    
    }

    /**
     * Test of compareTo method, of class Simbolo.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Simbolo simbolo = new Simbolo (null, "E", null);
        Simbolo instance = new Simbolo(null, "A", null);
        int expResult = -1;
        int result = instance.compareTo(simbolo);
        assertEquals(expResult, result);
    
    }


    /**
     * Test of equals method, of class Simbolo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Simbolo(".", "E", "LETTER");
        Simbolo instance = new Simbolo(null, "E", null);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    
    }
    
}
