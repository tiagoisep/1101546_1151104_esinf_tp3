/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ferreira
 */
public class TreeTest {

    Tree arvoreMorse = new Tree();
    Simbolo s0 = new Simbolo(null,"START","ROOT");
    Simbolo s1 = new Simbolo(".", "E", "LETTER");
    Simbolo s2 = new Simbolo("_", "T", "LETTER");
    Simbolo s3 = new Simbolo("..", "I", "LETTER");
    Simbolo s4 = new Simbolo("._", "A", "LETTER");
    Simbolo s5 = new Simbolo("_.", "N", "LETTER");
    Simbolo s6 = new Simbolo("__", "M", "LETTER");
    Simbolo s7 = new Simbolo("...","7","NUMBER");

    List<Simbolo> lista = new ArrayList<>();
    public TreeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        arvoreMorse.insert(s0);
        arvoreMorse.insert(s1,s1.getCodigoMorse());
        arvoreMorse.insert(s2,s2.getCodigoMorse());
        arvoreMorse.insert(s3,s3.getCodigoMorse());
        arvoreMorse.insert(s4,s4.getCodigoMorse());
        arvoreMorse.insert(s5,s5.getCodigoMorse());
        arvoreMorse.insert(s6,s6.getCodigoMorse());
        arvoreMorse.insert(s7,s7.getCodigoMorse());

        
        lista.add(s0);
        lista.add(s1);
        lista.add(s2);
        lista.add(s3);
        lista.add(s4);
        lista.add(s5);
        lista.add(s6);
        lista.add(s7);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of insert method, of class Tree.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        Object expResult = arvoreMorse.size();
        assertEquals(lista.size(), expResult);
    }
    

    /**
     * Test of searchElement method, of class Tree.
     */
    @Test
    public void testSearchElement() {
        System.out.println("searchElement");
        String procura = "...";
        Object expResult = s7;
        Object result = arvoreMorse.searchElement(procura);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of searchLetter method, of class Tree.
     */
    @Test
    public void testSearchLetter() {
        System.out.println("searchLetter");
        Simbolo sLetter=new Simbolo(null, "E", null);
        Object expResult = s1;
        Object result = arvoreMorse.searchLetter(sLetter);
        assertEquals(expResult, result);
    
    }

    /**
     * Test of caminhoComunm method, of class Tree.
     */
    @Test
    public void testCaminhoComunm() {
        System.out.println("caminhoComunm");
        String ss5 = s5.getCodigoMorse();
        String ss6 = s6.getCodigoMorse();
        Object expResult = s2;
        Object result = arvoreMorse.caminhoComunm(ss5, ss6);
        assertEquals(expResult, result);
    }

}
