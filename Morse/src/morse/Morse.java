/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Ana Cerqueira
 * @author Tiago Ferreira
 */
public class Morse {

    public static final String SEPARADOR = " ";

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("#########MORSE CODE PROGRAM#########");
        System.out.println("");
        
       //1. Criar a árvore BST de descodificação código morse partindo do ficheiro fornecido
       Tree<Simbolo> arvoreMorse = new Tree();
       Lerficheiro.lerFicheiro(arvoreMorse);
       System.out.println("ARVORE ALINEA 1");
       System.out.println(arvoreMorse.toString());
       System.out.println("");

        //2. Descodificar uma sequência de um código morse para a correspondente representação alfanumérica
        String codigoADescodificar = ". ... .. _. .._.";
        
        String[] codigoToChar = codigoADescodificar.split(SEPARADOR);
        StringBuilder palavra = new StringBuilder();

        for (int i = 0; i < codigoToChar.length; i++) {
            Simbolo s = arvoreMorse.searchElement(codigoToChar[i]);
            String letra = s.getRepresentacaoAlfanumerica();
            palavra.append(letra);
        }

        System.out.println("DESCODIFICAÇAO ALINEA 2");
        System.out.print(codigoADescodificar +"   ");
        System.out.println(palavra);
        System.out.println("");
        
        
        
        
        //3. Utilizando a BST da alínea 1) construir uma nova BST para codificação morse apenas das letras “A” a “Z”
        Tree<Simbolo> treeLetter = new Tree();
        Simbolo cd = new Simbolo(null, "START", "ROOT");
        treeLetter.insert(cd);
        Map<Integer, List<Simbolo>> map = arvoreMorse.nodesByLevel();   //O(n)
        for (Map.Entry<Integer, List<Simbolo>> entry : map.entrySet()) { //O(n)
            Integer key = entry.getKey();   //O(1)
            List<Simbolo> value = entry.getValue();   //O(1)

            for (Simbolo code : value) {    //O(n)
                if (code.isLetter()) {   //O(1)
                    treeLetter.insert(code, code.getCodigoMorse());     //O(logn))
                }
            }
        }
        
        
        System.out.println("ARVORE ALINEA 3");
        System.out.println(treeLetter.toString());
        

        // 4. Utilizando a BST da alínea 3) Efetuar a codificação morse de palavras completas só com letras
        String codigoACodificar = "DIOGO";
        int iteracoes = codigoACodificar.length();
        StringBuilder codigoElaborado = new StringBuilder();

        for (int i = 0; i < iteracoes; i++) {
            String letra = codigoACodificar.substring(0, 1);
            Simbolo s = new Simbolo(null, letra, "Letter");

            Simbolo sEncontrado = treeLetter.searchLetter(s);
            if (sEncontrado != null) {
                String codigo = sEncontrado.getCodigoMorse();
                codigoElaborado.append(codigo);
                codigoElaborado.append(" ");
            }

            codigoACodificar = codigoACodificar.substring(1);
        }
        System.out.println("CODIFICAÇÃO ALINEA 4");
        System.out.print("Diogo: ");
        System.out.println(codigoElaborado);
        System.out.println("");
        
        // 5. Com recurso à BST criada na alínea 1), encontrar a sequência inicial de “pontos” e “traços” comum a dois
        //símbolos (ex.: entre 5 “.....” e 4 “...._” a sequência “....” é comum)
        System.out.println("\nALINEA 5");
        Simbolo simbolo1= new Simbolo("._.", "R", "LETTER");
        Simbolo simbolo2= new Simbolo(".__", "W", "LETTER");
        String ss1 = simbolo1.getCodigoMorse();
        String ss2 = simbolo2.getCodigoMorse();
        Simbolo retorno = arvoreMorse.caminhoComunm(ss1, ss2);
        System.out.println("Caminho comum entre dois simbolos");
        System.out.println(retorno.getCodigoMorse());
        
        
        
        //6. Usando estruturas BST, filtrar e ordenar uma lista de palavras em código morse por total de ocorrências de tipo de classe de símbolos que a compõem
        List<String> listaPalavras = new ArrayList<>();
        List<Palavra> novaListaPalavra = new ArrayList<>();
        String s1 = ".____ ..___ ...__";
        String s2 = "._ ..___ ...__";
        String s3 = "._ _... ...__";
        String s4 = "._ _... _._.";
        String s5 = "_._.__ ..___ _._.";
        String s6 = "_._.__ ._.._. _._.";
        String s7 = "_._.__ ._.._. _..._";
        listaPalavras.add(s1);
        listaPalavras.add(s2);
        listaPalavras.add(s3);
        listaPalavras.add(s4);
        listaPalavras.add(s5);
        listaPalavras.add(s6);
        listaPalavras.add(s7);
        for (String word : listaPalavras) {
            StringBuilder representacaoPalavra = new StringBuilder();
            String[] codigoMorseToArray = word.split(SEPARADOR);
            int[] classificaChar = new int[4]; //pos 0 - letra; pos 1 - non-english ; pos 2 - numero ; pos 3 - pontuacao

            for (int i = 0; i < codigoMorseToArray.length; i++) {
                Simbolo sPrcurado = arvoreMorse.searchElement(codigoMorseToArray[i]);
                sPrcurado.classificaChar(classificaChar);
                representacaoPalavra.append(sPrcurado.getRepresentacaoAlfanumerica());

            }
            Palavra p = new Palavra(word, representacaoPalavra, classificaChar[0], classificaChar[1],
                    classificaChar[2], classificaChar[3]);
            novaListaPalavra.add(p);
        }
        
        System.out.println("\nALINEA 6");
        System.out.println("");
        int i=0;
        for (Palavra p : novaListaPalavra) {
            System.out.println("Código morse: " + listaPalavras.get(i));
            System.out.print("Representaçao alfanumérica: " + p.getRepresentacaoAlfaNum());
            System.out.print("\t\tLetra: " + p.getLetras());
            System.out.print(" Non-English: " + p.getNonEnglish());
            System.out.print(" Numero: " + p.getNumero());
            System.out.println(" Pontuação: " + p.getPontuacao());
            System.out.println("");
            i++;
        }

        Comparator<Palavra> criterioLetra = new Comparator<Palavra>() {
            @Override
            public int compare(Palavra p1, Palavra p2) {
                if (p1.getLetras() > p2.getLetras()) {
                    return 1;
                }
                if (p1.getLetras() < p2.getLetras()) {
                    return -1;
                }
                return 0;
            }
        };
        Comparator<Palavra> criterioNonEnglish = new Comparator<Palavra>() {
            @Override
            public int compare(Palavra p1, Palavra p2) {
                if (p1.getNonEnglish() > p2.getNonEnglish()) {
                    return 1;
                }
                if (p1.getNonEnglish() < p2.getNonEnglish()) {
                    return -1;
                }
                return 0;
            }
        };

        Comparator<Palavra> criterioNumber = new Comparator<Palavra>() {
            @Override
            public int compare(Palavra p1, Palavra p2) {
                if (p1.getNumero() > p2.getNumero()) {
                    return 1;
                }
                if (p1.getNumero() < p2.getNumero()) {
                    return -1;
                }
                return 0;
            }
        };

        Comparator<Palavra> criterioPonctuation = new Comparator<Palavra>() {
            @Override
            public int compare(Palavra p1, Palavra p2) {
                if (p1.getPontuacao() > p2.getPontuacao()) {
                    return 1;
                }
                if (p1.getPontuacao() < p2.getPontuacao()) {
                    return -1;
                }
                return 0;
            }
        };

        Collections.sort(novaListaPalavra, Collections.reverseOrder(criterioLetra));
        System.out.println("Lista ordenada por ocorrencias do tipo Letter");
        for (Palavra palv : novaListaPalavra) {
            if (palv.getLetras() != 0) {
                System.out.print(palv.getRepresentacaoAlfaNum() + " ");
            }

        }
        System.out.println("");
        
        Collections.sort(novaListaPalavra, Collections.reverseOrder(criterioNonEnglish));
        System.out.println("Lista ordenada por ocorrencias do tipo Non-English");
        for (Palavra otherWord : novaListaPalavra) {
            if (otherWord.getNonEnglish()!= 0) {
                System.out.print(otherWord.getRepresentacaoAlfaNum() + " ");
            }
        }
        System.out.println("");
        
        Collections.sort(novaListaPalavra, Collections.reverseOrder(criterioNumber));
        System.out.println("Lista ordenada por ocorrencias do tipo Number");
        for (Palavra otherWord : novaListaPalavra) {
            if (otherWord.getNumero() != 0) {
                System.out.print(otherWord.getRepresentacaoAlfaNum() + " ");
            }
        }
        
        System.out.println("");
        Collections.sort(novaListaPalavra, Collections.reverseOrder(criterioPonctuation));
        System.out.println("Lista ordenada por ocorrencias do tipo Ponctuation");
        for (Palavra otherWord : novaListaPalavra) {
            if (otherWord.getPontuacao()!= 0) {
                System.out.print(otherWord.getRepresentacaoAlfaNum() + " ");
            }
        }
        System.out.println("");
    }
}
