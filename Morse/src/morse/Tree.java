/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import java.util.Iterator;

/**
 * @author Ana Cerqueira
 * @author Tiago Ferreira
 * @param <E>
 */
public class Tree<E extends Comparable<E>> extends BST<E> {

    /**
     * Método que insere um simbolo numa arvore atraves de um caminho
     *
     * @param element
     * @param caminho
     */
    public void insert(E element, String caminho) { //O(log(n))
        root = insert(element, root, caminho); //O(log(n))
    }

    /**
     * Método que insere um simbolo numa arvore atraves de um caminho
     *
     * @param element
     * @param node
     * @param caminho
     * @return um node
     */
    private Node<E> insert(E element, Node<E> node, String caminho) {   //O(log(n))
        if (node == null) {  //O(1)
            return new Node(element, null, null);  //O(1)
        }

        char caracter = caminho.charAt(0);  //O(1)

        if (caminho.length() == 0) {   //O(1)
            node.setElement(element);  //O(1)
        } else {
            if (caracter == '.') {  //O(1)
                node.setLeft(insert(element, node.getLeft(), caminho.substring(1)));   //O(log(n))
            } else {
                node.setRight(insert(element, node.getRight(), caminho.substring(1))); //O(log(n))
            }
        }
        return node;  //O(1)
    }

    /**
     * Método que procura um elemento segundo um caminho
     *
     * @param procura
     * @return um elemento
     */
    public E searchElement(String procura) {
        Node<E> wantedNode = searchRecElement(root, procura);   //O(log(n))
        if (wantedNode != null) {   //O(1)
            return wantedNode.getElement();   //O(1)
        }
        return null;   //O(1)
    }

    /**
     * Metodo que procura um elemento segundo um caminho (Recursivamente)
     *
     * @param node
     * @param procura
     * @return um elemento
     */
    private Node<E> searchRecElement(Node<E> node, String procura) {    //O(log(n))
        Node<E> aNode = node;   //O(1)

        if (procura.length() == 0) { //O(1)
            return aNode;   //O(1)
        }

        char aux = procura.charAt(0);   //O(1)

        if (aux == '.') {   //O(1)
            aNode = searchRecElement(node.getLeft(), procura.substring(1));   //O(log(n))
        } else if (aux == '_') {
            aNode = searchRecElement(node.getRight(), procura.substring(1));   //O(log(n))
        }
        return aNode;   //O(1)
    }

    /**
     * Método que procura elementos do tipo letra
     *
     * @param element
     * @return um elemento do tipo letra
     */
    public E searchLetter(E element) { //O(n)
        boolean stop = false; //O(1)
        Iterable<E> it = inOrder(); //O(1)
        Iterator iteradorArvore = it.iterator(); //O(1)

        while (iteradorArvore.hasNext() && !stop) { //O(n)
            E a = (E) iteradorArvore.next(); //O(1)
            stop = a.equals(element); //O(1)
            if (stop) { //O(1)
                return a; //O(1)
            }

        }
        return null; //O(1)
    }

    /**
     * Método que procura o caminho comum entre dois simbolos
     *
     * @param s1
     * @param s2
     * @return o elemento comum entre os dois simbolos
     */
    public E caminhoComunm(String s1, String s2) { //O(log(n))
        Node<E> morse = caminhoComunmRec(root, s1, s2); //O(log(n))
        return morse.getElement(); //O(1)
    }

    /**
     * Método que procura uma caminho comum entre dois simbolos passados por
     * parametro (Recursivamente)
     *
     * @param node
     * @param s1
     * @param s2
     * @return o elemento comum entre os dois simbolos
     */
    private Node<E> caminhoComunmRec(Node<E> node, String s1, String s2) {  //O(log(n))
        Node<E> morse = node; //O(1)
        if (s1.charAt(0) == '.' && s2.charAt(0) == '.') { //O(1)
            morse = caminhoComunmRec(node.getLeft(), s1.substring(1), s2.substring(1)); //O(log(n))
        } else if (s1.charAt(0) == '_' && s2.charAt(0) == '_') {
            morse = caminhoComunmRec(node.getRight(), s1.substring(1), s2.substring(1)); //O(log(n))
        }

        return morse; //O(1)
    }

}
