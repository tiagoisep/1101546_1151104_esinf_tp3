/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Ana Cerqueira
 * @author Tiago Ferreira
 */
public class Lerficheiro {

    public static final String SEPARADOR = " ";
    
    public static void lerFicheiro(Tree<Simbolo> arvoreMorse) throws FileNotFoundException {

        Scanner ler = new Scanner(new File("morse_v3.csv"));
        Simbolo s0 = new Simbolo(null, "START" ,"Root");
        arvoreMorse.insert(s0);
        while (ler.hasNext()) {
            String linha = ler.nextLine();
            if (linha.length() > 0) {
                String[] campo = linha.split(SEPARADOR);
                String caminho = campo[0];
                Simbolo s = new Simbolo(campo[0], campo[1], campo[2]);
                arvoreMorse.insert(s, caminho);
            }
        }
    }

}
