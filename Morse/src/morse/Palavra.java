/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import java.util.Collections;
import java.util.List;

/**
 * @author Ana Cerqueira
 * @author Tiago Ferreira
 */
public class Palavra {

    /**
     * o codigo morse de uma palavra
     */
    String codigo;

    /**
     * representação alfanumerica de uma palavra
     */
    StringBuilder representacaoAlfaNum;
    /**
     * numero de letras existentes numa palavra
     */
    int letras;

    /**
     * identificação de uma palavra não inglesa
     */
    int nonEnglish;

    /*
    * identificação de um numero
     */
    int numero;

    /**
     * identificação de pontuação
     */
    int pontuacao;

    /**
     * construtor completo, constói uma instancia de palavra
     *
     * @param codigo
     * @param representacao
     * @param letra
     * @param nonEnglish
     * @param numero
     * @param pontuacao
     */
    public Palavra(String codigo, StringBuilder representacao, int letra, int nonEnglish, int numero, int pontuacao) {
        this.codigo = codigo;
        this.representacaoAlfaNum = representacao;
        this.letras = letra;
        this.nonEnglish = nonEnglish;
        this.numero = numero;
        this.pontuacao = pontuacao;
    }

    /*
    * devolve um codigo morse
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     *
     * devolve uma instancia de stringbuilder
     *
     * @return representacaoAlfaNum
     */
    public StringBuilder getRepresentacaoAlfaNum() {
        return representacaoAlfaNum;
    }

    /**
     * devolve o numero de letras de uma palavra
     *
     * @return o numero de letras de uma palavra
     */
    public int getLetras() {
        return letras;
    }

    /**
     * devolve o numero de elementos Non-English de uma palavra
     * 
     * @return  o numero de elementos Non-English de uma palavra
     */
    public int getNonEnglish() {
        return nonEnglish;
    }

    /**
     * devolve o numero de caracteres que são do tipo Number de uma palavra
     * 
     * @return devolve o numero de caracteres que são do tipo Number de uma palavra
     */
    public int getNumero() {
        return numero;
    }

    /**
     * devolve o numero de caracteres do tipo pontuação 
     * @return o numero de caracteres do tipo pontuação 
     */
    public int getPontuacao() {
        return pontuacao;
    }
}
