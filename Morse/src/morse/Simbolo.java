/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse;

import java.util.Objects;

/**
 * @author Ana Cerqueira
 * @author Tiago Ferreira
 */
public class Simbolo implements Comparable<Simbolo> {

    /**
     * Código morse do simbolo, atriburo de simbolo
     */
    private String codigoMorse;
    /**
     * Representação alfanumerica, atriburo de simbolo
     */
    private String representacaoAlfanumerica;
    /**
     * Classe do simbolo, atriburo de simbolo
     */
    private String classeDeSimbolo;

    /**
     * Construtor completo: permite criar uma instância de simbolo recebendo por
     * parametro uma string com a representação alfanumerica do simbolo e uma
     * string com a sua classe
     *
     * @param codigo
     * @param representacao
     * @param classeSimbolo
     */
    public Simbolo(String codigo, String representacao, String classeSimbolo) {
        this.codigoMorse = codigo;
        this.representacaoAlfanumerica = representacao;
        this.classeDeSimbolo = classeSimbolo;
    }

    /**
     * Método que devolve a ro código morse do simbolo
     *
     * @return codigoMorse
     */
    public String getCodigoMorse() {
        return codigoMorse;
    }

    /**
     * Método que devolve a representação alfanumérica do simbolo
     *
     * @return representacaoAlfanumerica
     */
    public String getRepresentacaoAlfanumerica() {
        return representacaoAlfanumerica;
    }

    /**
     * Método que devolve a classe do simbolo
     *
     * @return classeDeSimbolo
     */
    public String getClasseDeSimbolo() {
        return classeDeSimbolo;
    }

    /**
     * Método que devolve uma string com a representação alfanumérica e com a
     * classe do simbolo
     *
     * @return string com a representação alfanumérica e com a classe do simbolo
     */
    @Override
    public String toString() {
        return "[" + representacaoAlfanumerica + "]";
    }
/**
 * Método que avalia se um objeto é do tipo letra
 * @return se um objeto é do tipo letra 
 */
    public boolean isLetter() {
        return (this.classeDeSimbolo.equalsIgnoreCase("Letter"));
    }
/**
 * Método que avalia se um objeto é do tipo number
 * @return se um objeto é do tipo number
 */
    public boolean isNumber() {
        return (this.classeDeSimbolo.equalsIgnoreCase("Number"));
    }

    /**
     * Método que avalia se um objeto é do tipo non-english
     * @return se um objeto é do tipo non-english
     */
    public boolean isNonEnglish() {
        return (this.classeDeSimbolo.equalsIgnoreCase("Non-English"));
    }

    /**
     * Método que classifica um char quanto ao seu tipo
     * @param classificaChar 
     */
    public void classificaChar(int[] classificaChar) {
        boolean letra = this.isLetter();
        boolean nonEnglish = this.isNonEnglish();
        boolean numero = this.isNumber();
        if (letra) {
            classificaChar[0]++;
        } else if (nonEnglish) {
            classificaChar[1]++;
        } else if (numero) {
            classificaChar[2]++;
        } else {
            classificaChar[3]++;
        }
    }

    /**
     * Método que permite verificar se um deterlinado elemento pertence à classe
     * de "Letter".
     *
     * @param simbolo
     * @return 0 caso a classe do simbolo seja uma
     */
    @Override
    public int compareTo(Simbolo simbolo) {
        if (this.representacaoAlfanumerica.compareToIgnoreCase(simbolo.representacaoAlfanumerica) < 0) {
            return -1;
        } else if (this.representacaoAlfanumerica.compareToIgnoreCase(simbolo.representacaoAlfanumerica) > 0) {
            return 1;

        } else {
            return 0;
        }

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.representacaoAlfanumerica);
        return hash;
    }

    /**
     * Método que verifica se um objeto é igual a outro passado por parametro
     * @param obj
     * @return boolean true - é igual false - não é igual
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Simbolo other = (Simbolo) obj;
        if (!Objects.equals(this.representacaoAlfanumerica, other.representacaoAlfanumerica)) {
            return false;
        }
        return true;
    }

}
